use std::sync::mpsc::{channel, Sender, Receiver};
use std::collections::HashMap;
use std::io::prelude::*;
use std::io::BufReader;
use std::fs::File;
use std::path::Path;
use std::ops::{Deref, DerefMut};
use std::rc::Rc;
use std::time::Duration;
use std::thread;

use rand::Rng;

use irc::*;

//const SYMBOLS: &'static str = ",;:\"";
//const FINAL_PERIODS: &'static str = ".?!";

#[derive(Debug)]
struct Words {
    link: HashMap<Rc<String>, Option<Box<Words>>>,
}


impl Deref for Words {
    type Target = HashMap<Rc<String>, Option<Box<Words>>>;
    
    fn deref(&self) -> &HashMap<Rc<String>, Option<Box<Words>>> {
        &self.link
    }
}

impl DerefMut for Words {
    fn deref_mut<'a>(&'a mut self) -> &'a mut HashMap<Rc<String>, Option<Box<Words>>> {
        &mut self.link
    }
}

fn split_line(line: String) -> Vec<String> {
    line.to_lowercase()
        .split_whitespace()
        .map(String::from)
        .collect()
}

impl Words {
    fn new(word: String) -> Words {
        let mut map = HashMap::new();
        map.insert(Rc::new(word), None);
        
        Words {
            link: map,
        }
    }

    fn parse_line(&mut self, line: String) {
        struct ParseEnv {
            words: Vec<String>,
        }

        fn sub_parse_line(mut env: ParseEnv, brain: &mut Words) {
            if let Some(word) = env.words.pop() {
                let entry = brain.entry(Rc::new(word))
                                 .or_insert_with(|| env.words.pop()
                                                       .and_then(|x| Some(Box::new(Words::new(x)))));
                match *entry {
                    Some(ref mut b) => sub_parse_line(env, b),
                    _ => {},
                }
            }
        };

        let env = {
            let mut words = split_line(line);
            words.reverse();
            ParseEnv { words: words }
        };
        sub_parse_line(env, self);
    }

    fn get_random_phrase(&self) -> String {
        use rand::{thread_rng, sample};
        let mut rng = thread_rng();
        let mut phrase = Vec::new();
        let mut branch = &*self;

        //let mut current_word = sample(&mut rng, base.into_iter(), 1)[0].clone();
        let mut curr_word = {
            let mut keys = sample(&mut rng, branch.keys(), 1);
            if !keys.is_empty() {
                keys.pop().unwrap()
            } else {
                // Should it panic here?
                return String::new()
            }
        };

        while let Some(next) = branch.get(curr_word) {
            phrase.push(curr_word.to_string());
            curr_word = {
                match *next {
                    Some(ref map) => {
                        branch = map;
                        let mut keys = sample(&mut rng, map.keys(), 1);
                        if !keys.is_empty() {
                            keys.pop().unwrap()
                        } else {
                            break
                        }
                    },
                    None => break,
                }
            };
        }
        phrase.join(" ")
    }
}

pub fn engine(rx: Receiver<Message>, tx: Sender<Message>) {
    let mut brain = Words::new(String::new());

    match File::open(Path::new("brain.txt")) {
        Ok(f) => {
            let reader = BufReader::new(f);
            for line in reader.lines() {
                brain.parse_line(line.unwrap());
            }
        },
        Err(e) => panic!("{:?}", e),
    }
    
    loop {
        match rx.recv() {
            Ok(Message::Privmsg {
                from,
                to,
                msg: message,
            }) => {
                if message == ":q sona" {
                    break
                }
                brain.parse_line(message.replace("sona", &from));
                let phrase = get_random_phrase(&brain);
                tx.send(Message::Privmsg {
                    from: "sona".to_string(),
                    to: to,
                    msg: phrase,
                }).unwrap();
            },
            _ => {},
        }
    }
}
