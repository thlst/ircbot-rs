// RFC 2812 (https://tools.ietf.org/html/rfc2812).

use std::io::prelude::*;
use std::io;
use std::cell::RefCell;
use std::net::TcpStream;
use std::rc::Rc;
use std::iter::Iterator;
use std::str::FromStr;

use regex::Regex;

// TODO: Create an error type
pub type Result<T> = ::std::result::Result<T, String>;

pub struct Incoming<'a> {
    stream: &'a Irc2812,
}

#[derive(Debug)]
pub enum Message {
    Privmsg { from: String, to: String, msg: String },
    Ping { msg: String },
    Pong { msg: String },
    Join { channel: String },
    Pass { pw: Option<String> },
    Nick { nick: String },
    User { usr: String, mode: Option<String>, unused: String, rn: Option<String> },
    Oper { name: String, pw: String },
    Mode { nick: String, md: String },
    Quit { reason: Option<String> },
    Part { channel: String, reason: Option<String> },
}

impl FromStr for Message {
    // message    =  [ ":" prefix SPACE ] command [ params ] crlf
    // prefix     =  servername / ( nickname [ [ "!" user ] "@" host ] )
    // command    =  1*letter / 3digit
    // params     =  *14( SPACE middle ) [ SPACE ":" trailing ]
    //            =/ 14( SPACE middle ) [ SPACE [ ":" ] trailing ]
    
    type Err = ();
    
    fn from_str(s: &str) -> ::std::result::Result<Message, ()> {
        if s.is_empty() {
            return Err(());
        }

        let arg_nth = {
            let colon_line: String = s.trim_left_matches(':')
                                      .chars()
                                      .take_while(|&x| x != '\r')
                                      .collect();

            let idx = colon_line.find(':')
                                .unwrap_or(colon_line.len() - 1);

            let (left, right) = colon_line.split_at(idx);
            let mut args = left.split_whitespace()
                               .map(|s| s.to_string())
                               .collect::<Vec<_>>();

            args.push(right[1..].to_string());
            move |idx: usize| args[idx].clone()
        };

        // If there's a colon at the beginning, then 
        // some command is talking directly to the user.
        // Otherwise, it's a normal message command.
        if s.as_bytes()[0] == b':' {
            let nick = arg_nth(0).chars()
                                .take_while(|&x| x != '!')
                                .collect::<String>();

            let command = &*arg_nth(1);
            match command {
                "PRIVMSG" => {
                    let ret = Message::Privmsg {
                        from: nick,
                        to: arg_nth(2),
                        msg: arg_nth(3),
                    };
                    Ok(ret)
                },
                _ => Err(()),
            }
        } else {
            let command = &*arg_nth(0);
            match command {
                "PING" => {
                    let cmd = Message::Ping {
                        msg: arg_nth(1),
                    };
                    Ok(cmd)
                },
                _ => Err(()),
            }
        }
    }
}

impl<'a> Iterator for Incoming<'a> {
    type Item = Message;

    fn next(&mut self) -> Option<Message> {
        let buf = self.stream.recv();
        let msg = buf.parse::<Message>();
        msg.ok()
    }
}


// A socket wrapper 
pub struct Irc2812(Rc<RefCell<TcpStream>>);

impl Irc2812 {
    pub fn new(stream: TcpStream) -> Irc2812 {
        Irc2812(Rc::new(RefCell::new(stream)))
    }

    fn send(&self, line: String) {
        let stream = self.0.clone();
        let mut stream = stream.borrow_mut();
        stream.write_all(line.as_bytes()).unwrap();
        stream.flush().unwrap();
    }

    pub fn recv(&self) -> String {
        let mut stream = self.0.borrow_mut();
        let mut buf = [0; 512];
        match stream.read(&mut buf) {
            Err(_) => return String::new(),
            _ => {},
        }
        let buf = ::std::str::from_utf8(&buf).unwrap();
        buf.to_string()
    }

    pub fn iter(&self) -> Incoming {
        Incoming { stream: self }
    }

    pub fn priv_msg(&self, to: String, message: String) {
        self.send(format!("PRIVMSG {} :{}\r\n", to, message));
    }

    pub fn ping(&self, info: String) {
        self.send(format!("PING :{}\r\n", info));
    }

    pub fn pong(&self, info: String) {
        self.send(format!("PONG :{}\r\n", info));
    }

    pub fn join(&self, channel: String) {
        self.send(format!("JOIN {}\r\n", channel));
    }

    pub fn pass(&self, pw: Option<String>) {
        self.send(format!("PASS {}\r\n", pw.unwrap_or(String::new())));
    }

    pub fn nick(&self, nick: String) {
        self.send(format!("NICK {}\r\n", nick));
    }

    pub fn user(&self, user: String,
                mode: Option<String>,
                unused: Option<String>,
                realname: Option<String>)
    {
        self.send(format!("USER {us} {md} {un} :{rn}\r\n",
                          us = user.clone(),
                          md = mode.unwrap_or("0".to_string()),
                          un = unused.unwrap_or("*".to_string()),
                          rn = realname.unwrap_or(user)));
    }

    pub fn oper(&self, name: String, passwd: String) {
        self.send(format!("OPER {} {}\r\n", name, passwd));
    }

    pub fn mode(&self, nick: String, md: String) -> Result<()> {
        let re = Regex::new("[+-][iwoOr]+").unwrap();
        let cmd = format!("MODE {} {}\r\n", nick, &md);
        if re.is_match(&md) {
            self.send(cmd.clone());
            Ok(())
        } else {
            Err(format!("command error: `{}` isn't a valid command.", cmd))
        }
    }

    // TODO
    pub fn service(&self) {
        unimplemented!()
    }

    pub fn quit(&self, reason: Option<String>) {
        self.send(format!("QUIT :{}\r\n", reason.unwrap_or(String::new())));

    }

    pub fn part(&self, channel: String, reason: Option<String>) {
        self.send(format!("PART {} :{}\r\n",
                          channel,
                          reason.unwrap_or(String::new())));
    }
}
