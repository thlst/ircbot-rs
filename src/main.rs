extern crate libc;
extern crate regex;
extern crate rand;

use std::io::prelude::*;
use std::io;
use std::net::TcpStream;
use std::thread;
use std::time::Duration;
use std::sync::mpsc;

use irc::{Irc2812, Message, Incoming};

mod irc;
mod ai;

fn main() {
    // thread::sleep(Duration::new(1, 0));

    let stream = TcpStream::connect("chat.freenode.net:6667").unwrap();
    stream.set_read_timeout(Some(Duration::new(1, 0))).unwrap();

    let irc = Irc2812::new(stream);
    let mut incoming = irc.iter();

    irc.pass(None);
    irc.nick("sona".to_string());
    irc.user("sona".to_string(), None, None, None);
    println!("{}", irc.recv());
    thread::sleep(Duration::new(10, 0));
    if let Some(Message::Ping { msg }) = incoming.next() {
        println!("ping! {}", msg);
        irc.pong(msg);
    }

    println!("{}", irc.recv());
    println!("{}", irc.recv());


    let channel: String = "#rustin_baby".to_string();
    
    irc.join(channel.clone());

    let (t_ai, rx) = mpsc::channel();
    let (tx, r_ai) = mpsc::channel();
    
    thread::spawn(move || ai::engine(r_ai, t_ai));

    tx.send(Message::Privmsg {
        from: String::new(),
        to: String::new(),
        msg: "hello".to_string(),
    }).unwrap();
    
    loop {
        let line = incoming.next();
        if line.is_some() {
            println!("{:?}", line);
        }
        match line {
            Some(Message::Ping { msg }) => {
                println!("ping! {}", msg);
                irc.pong(msg);
            },
            Some(msg) => {
                tx.send(msg).unwrap();
            },
            _ => {},
        }

        match rx.try_recv() {
            Ok(Message::Privmsg { msg, .. }) => irc.priv_msg(channel.clone(), msg),
            _ => {},
        }
    }
}
